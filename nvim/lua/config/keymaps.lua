-- Keymaps are automatically loaded on the VeryLazy event
-- Add any additional keymaps here
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/configvim.keymap.setymaps.lua

vim.keymap.set("n", "<M-Down>", ":m .+1<cr>==", { desc = "Move down" })
vim.keymap.set("n", "<M-Up>", ":m .-2<cr>==", { desc = "Move up" })
vim.keymap.set("i", "<M-Down>", "<esc>:m .+1<cr>==gi", { desc = "Move down" })
vim.keymap.set("i", "<M-Up>", "<esc>:m .-2<cr>==gi", { desc = "Move up" })
vim.keymap.set("v", "<M-Down>", ":m '>+1<cr>gv=gv", { desc = "Move down" })
vim.keymap.set("v", "<M-Up>", ":m '<-2<cr>gv=gv", { desc = "Move up" })
