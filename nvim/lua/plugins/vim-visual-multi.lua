return {
  "mg979/vim-visual-multi",
  version = "*",
  init = function()
    vim.g.VM_mouse_mappings = 1
  end,
}
