return {
  -- disabled
  { "echasnovski/mini.animate", enabled = false },
  {
    "echasnovski/mini.indentscope",
    opts = {
      draw = {
        animation = require("mini.indentscope").gen_animation.none(),
      },
    },
  },
}
