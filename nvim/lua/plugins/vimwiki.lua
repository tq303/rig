return {
  "vimwiki/vimwiki",
  -- init is called during startup. Configuration for vim plugins typically should be set in an init function
  init = function()
    vim.g.vimwiki_syntax = "markdown"
    vim.g.vimwiki_ext = ".md"
    vim.g.vimwiki_automatic_nested_syntaxes = 1
    vim.g.vimwiki_global_ext = 0

    local wiki = {}
    wiki.path = "~/projects/wiki"
    wiki.ext = ".md"
    wiki.nested_syntaxes = { tsx = "typescriptreact", ts = "typescript" }

    vim.g.vimwiki_list = { wiki }
  end,
}
