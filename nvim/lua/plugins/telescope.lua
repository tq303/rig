-- file & content search panel
-- https://github.com/nvim-telescope/telescope.nvim/issues/1048#issuecomment-1679797700
local select_one_or_multi = function(prompt_bufnr)
  local picker = require("telescope.actions.state").get_current_picker(prompt_bufnr)
  local multi = picker:get_multi_selection()
  if not vim.tbl_isempty(multi) then
    require("telescope.actions").close(prompt_bufnr)
    for _, j in pairs(multi) do
      if j.path ~= nil then
        vim.cmd(string.format("%s %s", "edit", j.path))
      end
    end
  else
    require("telescope.actions").select_default(prompt_bufnr)
  end
end

return {
  {
    "nvim-telescope/telescope.nvim",
    keys = {
      -- add a keymap to browse files from root
      {
        "<leader>ff",
        function()
          require("telescope.builtin").find_files({ hidden = true })
        end,
        desc = "Find file (root)",
        noremap = true,
        silent = true,
      },
      -- add a keymap to browse files from root
      {
        "<leader>fF",
        function()
          require("telescope.builtin").find_files({ hidden = true, search_dirs = { vim.fn.expand("%:p:h") } })
        end,
        desc = "Find file (cwd)",
        noremap = true,
        silent = true,
      },
      -- add search in files from root
      {
        "<leader>fs",
        function()
          require("telescope.builtin").live_grep({ hidden = true, grep_visual_selection = true })
        end,
        desc = "Search files (root)",
        noremap = true,
        silent = true,
      },
      -- add search in files from root
      {
        "<leader>fS",
        function()
          require("telescope.builtin").live_grep({ hidden = true, search_dirs = { vim.fn.expand("%:p:h") } })
        end,
        desc = "Search files (cwd)",
        noremap = true,
        silent = true,
      },
    },

    -- change some options
    opts = {
      defaults = {
        layout_strategy = "horizontal",
        layout_config = { prompt_position = "top" },
        sorting_strategy = "ascending",
        winblend = 0,
        mappings = {
          i = {
            ["<CR>"] = select_one_or_multi,
          },
        },
      },
      pickers = {
        live_grep = {
          additional_args = function(opts)
            return { "--hidden" }
          end,
        },
      },
    },
  },

  -- add telescope-fzf-native
  {
    "telescope.nvim",
    dependencies = {
      "nvim-telescope/telescope-fzf-native.nvim",
      build = "make",
      config = function()
        require("telescope").load_extension("fzf")
      end,
    },
  },
}
