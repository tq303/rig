return {
  "folke/trouble.nvim",
  -- opts will be merged with the parent spec
  opts = {
    multiline = true,
    use_diagnostic_signs = true,
    width = 10,
  },
}
