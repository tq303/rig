let mapleader = "\<Space>"
let g:mapleader = "\<Space>"

" nvim
" general
nnoremap <leader>s :w<cr>

nnoremap <leader>q :q<cr>
nnoremap <leader>qs :wq<cr>

" editing
nnoremap <leader>ea ggvG$
nnoremap <leader>el :set wrap linebreak<cr>
nnoremap <leader>es :setlocal spell spelllang=en_gb
nnoremap <leader>er "_d

vmap <leader>ef  <Plug>(coc-format-selected)

" tabs
nnoremap <leader>w :bd<cr>
nnoremap <leader>wf :bd!<cr>
nnoremap <leader>wa :bufdo bd<cr>
nnoremap <leader>n :enew<cr>
nnoremap <leader>. :split<cr>
nnoremap <leader>/ :vsplit<cr>
nnoremap <leader>j :bprev<cr>
nnoremap <leader>k :bnext<cr>
"nnoremap <leader>h :tabm -1<cr>
"nnoremap <leader>l :tabm +1<cr>

" files
nnoremap <leader>o :Files<cr>
nnoremap <leader>i :Ag<cr>

" search
nnoremap <leader>fc :set hlsearch!<CR>

vnoremap <leader>fa y<cr>:Ag <C-r>0<cr>
vnoremap <leader>ff y/<C-r>0<cr>

"coc
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gt <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" nerdtree
nnoremap <leader>b :NERDTree<cr>
nnoremap <leader>bc :NERDTreeToggle %<cr>

" reset initrc
nnoremap <leader>rv :source $MYVIMRC<cr>
nnoremap <leader>rt :CocRestart<cr>


" move block
nnoremap <M-Up> :m .-2<cr>==
nnoremap <M-Down> :m .+1<cr>==

inoremap <M-Up> <Esc>:m .-2<cr>==gi
inoremap <M-Down> <Esc>:m .+1<cr>==gi

vnoremap <M-Up> :m '<-2<cr>gv=gv
vnoremap <M-Down> :m '>+1<cr>gv=gv

" transform

function! s:mixedcase(word)
  return substitute(s:camelcase(a:word),'^.','\u&','')
endfunction

function! s:camelcase(word)
  let word = substitute(a:word, '-', '_', 'g')
  if word !~# '_' && word =~# '\l'
    return substitute(word,'^.','\l&','')
  else
    return substitute(word,'\C\(_\)\=\(.\)','\=submatch(1)==""?tolower(submatch(2)) : toupper(submatch(2))','g')
  endif
endfunction

function! s:snakecase(word)
  let word = substitute(a:word,'::','/','g')
  let word = substitute(word,'\(\u\+\)\(\u\l\)','\1_\2','g')
  let word = substitute(word,'\(\l\|\d\)\(\u\)','\1_\2','g')
  let word = substitute(word,'[.-]','_','g')
  let word = tolower(word)
  return word
endfunction

function! s:uppercase(word)
  return toupper(s:snakecase(a:word))
endfunction

function! s:dashcase(word)
  return substitute(s:snakecase(a:word),'_','-','g')
endfunction

function! s:spacecase(word)
  return substitute(s:snakecase(a:word),'_',' ','g')
endfunction

function! s:dotcase(word)
  return substitute(s:snakecase(a:word),'_','.','g')
endfunction

function! s:titlecase(word)
  return substitute(s:spacecase(a:word), '\(\<\w\)','\=toupper(submatch(1))','g')
endfunction

function! s:replace_selected_text(replace)
    return "hello"
endfunction

nnoremap <leader>ts :exe "norm! ciw" . <SID>snakecase(expand("<cword>"))<CR>

nnoremap <leader>tc :exe "norm! ciw" . <SID>camelcase(expand("<cword>"))<CR>
" test_input
" camel selection
" s#\%V\(\%(\<\l\+\)\%(_\)\@=\)\|_\(\l\)\%V#\u\1\2#g
" snake selection
" s#\%V\C\(\<\u[a-z0-9]\+\|[a-z0-9]\+\)\(\u\)\%V#\l\1_\l\2#g
