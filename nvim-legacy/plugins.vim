" Airline

" theme
let g:airline_theme='gruvbox'
" enable tabline
let g:airline#extensions#tabline#enabled = 1
" show numbered buffers
let g:airline#extensions#tabline#buffer_idx_mode = 1

colorscheme gruvbox

" coc
let s:coc_extensions = [
	\ 'coc-tsserver',
	\ 'coc-dictionary',
    \ 'coc-pairs',
	\ 'coc-json',
    \ 'coc-prettier',
	\ 'coc-eslint',
	\ 'coc-yaml',
	"\ 'coc-toml',
    "\ 'coc-git',
    "\ 'coc-rls',
\]

for extension in s:coc_extensions
	call coc#add_extension(extension)
endfor

let g:coc_user_config = {}

let g:coc_user_config['coc.preferences.jumpCommand'] = ':vsp'

" prettier
let g:coc_user_config['coc.preferences.formatOnSaveFiletypes'] = [
    \'javascript',
    \'typescript',
    \'typescriptreact',
    \'typescript.tsx',
    \'json',
    \'yaml'
\]

" eslint
let g:coc_user_config['eslint.filetypes'] = ['javascript', 'javascriptreact', 'typescript', 'typescriptreact']

" https://github.com/neoclide/coc-eslint/issues/56#issuecomment-647296257
let g:coc_user_config['eslint.nodePath'] = './node_modules'

" vimwiki
let g:vimwiki_syntax = 'markdown'
let g:vimwiki_ext = '.md'
let g:vimwiki_automatic_nested_syntaxes = 1
let wiki = {}
let wiki.path = '~/projects/wiki'
let wiki.ext = '.md'
let wiki.nested_syntaxes = {'tsx': 'typescriptreact'}

" register wikis
let g:vimwiki_list = [wiki]

" fzf
let $FZF_DEFAULT_COMMAND = 'ag --path-to-ignore ~/.ignore --hidden -g "" --ignore-dir node_modules'
let $FZF_DEFAULT_OPTS="--ansi --preview-window 'right:60%' --margin=1,4 --preview 'bat --theme=gruvbox --color=always --style=header,grid --line-range :300 {}'"

" vim multi cursor
let g:VM_mouse_mappings = 1

" tree
let NERDTreeShowHidden = 1

" rust
" let g:rustfmt_autosave = 1
