" general
set relativenumber              " show numbers relative to selected line
set incsearch                   " show search matches as you type
set showmode                    " always show what mode we're currently editing in
set autoread                    " auto refresh files that change whilst editing
set updatetime=300
set redrawtime=10000            " redraw page in case of updates
set nowrap                      " don't wrap lines
set clipboard=unnamed
set tabstop=4                   " four spaces
set softtabstop=4               " delete spaces as if tab
set expandtab                   " expand tabs to spaces
set shiftwidth=4                " number of spaces to use for autoindenting
set shiftround                  " use multiple of shiftwidth when indenting with '<' and '>'
set backspace=indent,eol,start  " delete these in insert mode
set autoindent                  " autoindent
set copyindent                  " copy previous indent
set number                      " always show line numbers
set ignorecase                  " ignore case when searching
set smartcase                   " ignore case if search pattern is all lowercase,
set timeout timeoutlen=500 ttimeoutlen=100
set visualbell                  " don't beep
set noerrorbells                " don't beep
set hidden                      " undo history
set mouse=a                     " enable mouse interaction
set hlsearch!                   " Highlight all search pattern matches"

set nocompatible
syntax on
filetype plugin indent on

au CursorHold,CursorHoldI * checktime " check file has changed on hold

" custom indent
au FileType md setlocal sw=2 sts=2 et
"autocmd FileType yml,yaml setlocal ts=2 sts=2 sw=2 expandtab
" au FileType cucumber setlocal sw=4 sts=4 et

" ensure syntax highlighting works on large files
au BufEnter *.{js,jsx,ts,tsx} :syntax sync fromstart
au BufLeave *.{js,jsx,ts,tsx} :syntax sync clear

" autocmd BufRead,BufWritePre *.sh normal gg=G

" https://vi.stackexchange.com/questions/454/whats-the-simplest-way-to-strip-trailing-whitespace-from-all-lines-in-a-file
fun! TrimWhitespace()
    let l:save = winsaveview()
    keeppatterns %s/\s\+$//e
    call winrestview(l:save)
endfun

au BufWritePre * :call TrimWhitespace()

