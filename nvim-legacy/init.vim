call plug#begin('~/.config/nvim/plugged')

" web
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'HerringtonDarkholme/yats.vim'

" rust
Plug 'rust-lang/rust.vim'

" fzf
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim', { 'do': 'brew install rg' }

" theme
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'morhetz/gruvbox'
Plug 'mhinz/vim-startify'

" shout out
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-cucumber'
Plug 'yaegassy/coc-cucumber', {'do': 'yarn install --frozen-lockfile'}

" navigaton
Plug 'preservim/nerdtree'

" selection
Plug 'mg979/vim-visual-multi', {'branch': 'master'}

" vimwiki
Plug 'vimwiki/vimwiki'

call plug#end()

source $HOME/.config/nvim/general.vim
source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/keys.vim

