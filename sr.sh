#!/bin/bash

# ____ _  _ _  _ ____ ___ _ ____ _  _ ____      /    _  _ ____ ____ ____
# |___ |  | |\ | |     |  | |  | |\ | [__      /     |  | |__| |__/ [__
# |    |__| | \| |___  |  | |__| | \| ___]    /       \/  |  | |  \ ___]

# confirm
YES="Yes"
NO="No"

# search variables
SR_SEARCH=""
SR_PATH=""
SR_IGNORE=""
SR_REPLACE=""
SR_ARGS=()

# conditionals
RUN_REPLACE=${NO}
RUN_VERIFY=${YES}
S_FILE=${NO}
S_DIR=${NO}

# colours
RED=$'\e[0;31m'
BGREEN=$'\e[1;32m'
GREEN=$'\e[0;32m'
YELLOW=$'\e[0;33m'
WHITE=$'\e[1;37m'
NC=$'\e[0m'
PURPLE=$'\e[0;35m'
BCYAN=$'\e[1;36m'
CYAN=$'\e[0;36m'

function usage {
	echo ${YELLOW}
	echo '|||||||||||||||||||||'
	echo '|| _______________ ||'
	echo '|| |______ |_____/ ||'
	echo '|| ______| |    \_ ||'
	echo '||                 ||'
	echo '|| search  replace ||'
	echo '|||||||||||||||||||||'
	echo ${NC}
	echo ${BCYAN}"Usage: ${NC}sr SEARCH [OPTIONS]"
	echo
	echo "   ${WHITE} SEARCH${NC} file/directory names or file content"
	echo "   ${WHITE} REPLACE${NC} pattern with something else"
	echo
	echo "    Uses silversearcher/find & sed"
	echo
	echo ${BCYAN}"Example: ${NC}sr foo -r bar"
	echo ${WHITE}
	echo "${WHITE}SEARCH:${NC} 'string' or 'regex' (required)"
	echo "${WHITE}REPLACE:${NC} 'string'"
	echo
	echo ${BCYAN}"Options:"
	echo ${NC}
	echo "${BGREEN}- d${NC}   Search for directories name."
	echo -n ${GREEN}
	echo "${BGREEN}- e${NC}   Exclude pattern."
	echo -n ${NC}
	echo "${BGREEN}- f${NC}   Search for files name."
	echo -n ${GREEN}
	echo "${BGREEN}- h${NC}   Show hidden."
	echo "${BGREEN}- i${NC}   Case insensitive."
	echo "${BGREEN}- l${NC}   List file names only. (exempt from options ${NC}f${GREEN}&${NC}d${GREEN})"
	echo "${BGREEN}- n${NC}   No verify."
	echo "${BGREEN}- p${NC}   Path/glob to search. Note glob must be in \"quotes\"."
	echo "${BGREEN}- r${NC}   Replace SEARCH with 'string'."
	echo "${BGREEN}- s${NC}   Case sensitive."
	echo "${BGREEN}- w${NC}   Whole word search."
	echo
	echo "Options follow ${WHITE}SEARCH${NC} argument"

}

function run_confirm() {
	confirm=${YES}

	if [[ ${RUN_VERIFY} == ${YES} ]]; then
		confirm=$(printf "${NO}\n${YES}" | fzf --header="Do you wish to continue?" --height="6")
	fi

	echo ${confirm}
}

function show_diff() {
	sed "s/\(.*\)${SR_SEARCH}/\1${RED}${SR_SEARCH} \~ ${GREEN}${SR_REPLACE}${WHITE}/"
}

function show_files() {
	echo -n ${WHITE}

	if [[ ${S_FILE} == ${YES} ]] || [[ ${S_DIR} == ${YES} ]]; then
		message=("The following")

		if [[ ${S_DIR} == ${YES} ]]; then message+=("directories"); fi
		if [[ ${S_FILE} == ${YES} ]] && [[ ${S_DIR} == ${YES} ]]; then message+=("and"); fi
		if [[ ${S_FILE} == ${YES} ]]; then message+=("files"); fi

		if [[ ${RUN_REPLACE} == ${YES} ]]; then
			message+=("will be replaced with:")
		else
			message+=("were found:")
		fi

		echo "${message[*]}"
		echo ${NC}

		for file in ${@}; do
			if [[ ${RUN_REPLACE} == ${YES} ]]; then
				echo "${file}" | show_diff
			else
				echo "${file}"
			fi
		done
	else
		echo "Changes will occur in the following files:"
		echo ${NC}
		echo ${@} | tr " " "\n"
	fi
}

function file_replace() {
	ignore=()
	search=()

	#ignore
	ignore+=("-path .*node_modules")

	if [[ ${SR_IGNORE} ]]; then
		ignore+=("-path ${SR_IGNORE}")
	fi

	#files/directories
	if [[ ${S_FILE} == ${YES} ]]; then
		search+=("-o -type f -name ${SR_SEARCH}* -print")
	fi

	if [[ ${S_DIR} == ${YES} ]]; then
		search+=("-o -type d -name ${SR_SEARCH}* -print")
	fi

	files=$(find ./${SR_PATH} ${ignore[*]} -prune ${search[*]})

	show_files ${files}

	if [[ ${RUN_REPLACE} == ${YES} ]]; then
		confirm=$(run_confirm)

		if [[ ${confirm} == ${YES} ]]; then
			for file in ${files}; do
				mv "${file}" "$(echo "${file}" | sed "s/\(.*\)${SR_SEARCH}/\1${SR_REPLACE}/")"
			done

			echo
			echo "replaced"
		else
			echo
			echo "cancelled"
		fi
	fi
}

function text_replace() {
	extra_args=()

	if [[ ${SR_IGNORE} ]]; then
		extra_args+=("--ignore ${SR_IGNORE}")
	fi

	if [[ ${RUN_REPLACE} == ${NO} ]]; then
		rg "${SR_SEARCH}" ${SR_PATH} ${SR_ARGS[*]} ${extra_args[*]} --stats
	else
		files=$(rg "${SR_SEARCH}" ${SR_PATH[*]} ${SR_ARGS[*]} ${extra_args[*]} -l)

		show_files ${files}

		confirm=$(run_confirm)

		if [[ ${confirm} == ${YES} ]]; then
			echo ${files} | xargs sed -i "" -e "s/${SR_SEARCH}/${SR_REPLACE}/g"

			echo
			echo "replaced"
		else
			echo
			echo "cancelled"
		fi
	fi
}

function error {
	echo ${RED}
	echo ${1}
	echo ${NC}
	echo "e.g. sr foo -p ./bar"
	echo
	echo "sr (for more information)"
}

# ____ ____ ___    ____ ___  ___ _ ____ _  _ ____
# | __ |___  |     |  | |__]  |  | |  | |\ | [__
# |__] |___  |     |__| |     |  | |__| | \| ___]

# no options show help
if [[ -z ${@} ]]; then
	usage
	exit 1
fi

# ensure SEARCH is first
if [[ ${1} != -* ]]; then
	SR_SEARCH=${1}
	shift

	if [[ ${1} != "" && ${1} != -* ]]; then
		error "Invalid SR_ARGS: ${1} is not allowed"
		exit 1
	fi
fi

# get argument options
while getopts :wlsihfdnr:p:e: opt; do
	case $opt in
	w) SR_ARGS+=(-${opt}) ;;
	l) SR_ARGS+=(-${opt}) ;;
	s) SR_ARGS+=(-${opt}) ;;
	i) SR_ARGS+=(-${opt}) ;;
	h) SR_ARGS+=(--hidden) ;;
	f) S_FILE=${YES} ;;
	d) S_DIR=${YES} ;;
	n) RUN_VERIFY=${NO} ;;
	r)
		SR_REPLACE="${OPTARG}"
		RUN_REPLACE=${YES}
		;;
	p) SR_PATH="${OPTARG}" ;;
	e) SR_IGNORE="${OPTARG}" ;;
	:)
		error "Invalid Option: -${OPTARG} requires an argument"
		exit 1
		;;
	\?) #invalid option
		;;
	esac
done

# remove options that have already been handled
shift $((OPTIND - 1))

# ensure SEARCH is present
if [[ ${SR_SEARCH} == "" ]]; then
	error "Invalid: Provide something to SR_SEARCH"
	exit 1
fi

# ____ _  _ ____ _ _ _    ____ _  _ ___ ___  _  _ ___
# [__  |__| |  | | | |    |  | |  |  |  |__] |  |  |
# ___] |  | |__| |_|_|    |__| |__|  |  |    |__|  |

if [[ ${@} ]]; then
	echo "${RED}Error found: ${@}${NC}"
fi

echo "SEARCH: ${GREEN}${SR_SEARCH}${NC}"

if [[ ${SR_REPLACE} ]]; then
	echo "REPLACE: ${RED}${SR_REPLACE}${NC}"
fi

if [[ ${SR_PATH} ]]; then
	echo "path: ${GREEN}${SR_PATH}${NC}"
fi

if [[ ${SR_ARGS} ]]; then
	echo "options: ${SR_ARGS[*]}"
fi

echo ${NC}

# ____ ____ ____ ____ ____ _  _    / ____ ____ ___  _    ____ ____ ____
# [__  |___ |__| |__/ |    |__|   /  |__/ |___ |__] |    |__| |    |___
# ___] |___ |  | |  \ |___ |  |  /   |  \ |___ |    |___ |  | |___ |___

if [[ ${S_FILE} == ${YES} ]] || [[ ${S_DIR} == ${YES} ]]; then
	file_replace
else
	text_replace
fi

# _ ____ ____ _  _ ____ ____
# | [__  [__  |  | |___ [__
# | ___] ___] |__| |___ ___]

# searching files with extension will also replace the the files extension
# need a way to check if pattern has a . for file search, if so search whole file

# ____ ____ ____ ___ _  _ ____ ____ ____
# |___ |___ |__|  |  |  | |__/ |___ [__
# |    |___ |  |  |  |__| |  \ |___ ___]

# add option to delete line
# add option to search as text, i.e fully escape regex with -t?
# allow regex groups with multiple replace arguments e.g sr /\"(+w)\"\: \"(+w)\"/ -r "module" -r "1.0.0" (sed -e s/find/replace/g -e s/find2/replace2/g)
# print total found & total files (improve)
# add option to search by filetype
