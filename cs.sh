#!/bin/bash

# cheat.sh
function cheatsh {
    languages="git js lua nodejs rust tmux"
    # choose language
    language=`printf $languages | tr " " "\n" | fzf`

    echo "cheat.sh\n"
    echo "language: ${language}"

    # input search query
    echo -n "search: "
    read search

    # escape search for url
    query=`printf ${search// /+}`

    url="cht.sh/${language}/${query}"

    echo $url
    curl $url
}

