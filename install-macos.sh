#!/bin/bash

# /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
# brew analytics off

curl -O https://raw.githubusercontent.com/mawww/kakoune/master/contrib/tmux-256color.terminfo
/usr/bin/tic -x ./tmux-256color.terminfo
rm ./tmux-256color.terminfo

# kitty terminal
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin

# can't believe it
brew install tmux
# bout time
brew install neovim
# get pip3
brew install python3
# get neovim python interface
pip3 install neovim
pip3 install pynvim
brew install glow
# file search
brew install the_silver_searcher
brew install ripgrep
brew install fd
brew install fzf

# vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# tools
# cat with syntax highlighting
brew install bat

# git tool
brew install lazygit

# starship prompt
brew install starship

# work
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash

npm install -g neovim
curl -o- -L https://yarnpkg.com/install.sh | bash -s -- --version 1.22.17

brew install mkcert

# media
brew install imagemagick

# rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

rustup component add rls rust-analysis rust-src
# brew install ffmpeg

# hide dock
defaults write com.apple.dock autohide-delay -float 0
killall Dock
# defaults delete com.apple.dock autohide-delay; killall Dock (undo)

# dock spacers
# defaults write com.apple.dock persistent-apps -array-add '{"tile-type"="small-spacer-tile";}' && killall Dock
# defaults write com.apple.dock persistent-apps -array-add '{tile-data={}; tile-type="spacer-tile";}' && killall Dock
